local inspect = require 'lib.inspect'
local struct = require 'lib.struct'

local GLOBAL_CONTEXT = 0x1C84A0
local SAVE_CONTEXT = 0x11A5D0

local CONTROLLER = 'P1'
local CONTROLLER_PREFIX = CONTROLLER .. ' '

local running = true
local play_macro = false
local movie = nil

local function expand_buttons(buttons)
    return {
        a = bit.band(bit.rshift(buttons, 15), 1) ~= 0,
        b = bit.band(bit.rshift(buttons, 14), 1) ~= 0,
        z = bit.band(bit.rshift(buttons, 13), 1) ~= 0,
        s = bit.band(bit.rshift(buttons, 12), 1) ~= 0,
        du = bit.band(bit.rshift(buttons, 11), 1) ~= 0,
        dd = bit.band(bit.rshift(buttons, 10), 1) ~= 0,
        dl = bit.band(bit.rshift(buttons, 9), 1) ~= 0,
        dr = bit.band(bit.rshift(buttons, 8), 1) ~= 0,
        l = bit.band(bit.rshift(buttons, 5), 1) ~= 0,
        r = bit.band(bit.rshift(buttons, 4), 1) ~= 0,
        cu = bit.band(bit.rshift(buttons, 3), 1) ~= 0,
        cd = bit.band(bit.rshift(buttons, 2), 1) ~= 0,
        cl = bit.band(bit.rshift(buttons, 1), 1) ~= 0,
        cr = bit.band(buttons, 1) ~= 0,
    }
end

local function parse_pad_input(file)
    return {
        buttons = struct.unpack('>H', file:read(2)),
        x = struct.unpack('>b', file:read(1)),
        y = struct.unpack('>b', file:read(1)),
    }
end

local function parse_movie_input(file)
    return {
        raw = parse_pad_input(file),
        pad_delta = struct.unpack('>H', file:read(2)),
    }
end

local function parse_movie_seed(file)
    return {
        frame_idx = struct.unpack('>I', file:read(4)),
        old_seed = struct.unpack('>I', file:read(4)),
        new_seed = struct.unpack('>I', file:read(4)),
    }
end

local function parse_macro(file)
    local n_input = struct.unpack('>I', file:read(4))
    local n_seed = struct.unpack('>I', file:read(4))

    local input_start = parse_pad_input(file)
    local inputs = {}
    local seeds = {}
    
    for i = 1, n_input do
        inputs[i] = parse_movie_input(file)
    end

    for i = 1, n_seed do
        seeds[i] = parse_movie_seed(file)
    end

    -- todo: movie_oca_input, movie_oca_sync, movie_room_load
    if file:seek() < file:seek('end') then
        print('warn: sync data present, not supported')
    end

    return {
        input_start = input_start,
        inputs = inputs,
        seeds = seeds,
    }
end

local function zu_adjust_joystick(v)
    if v < 0 then
        if v > -8 then
            return 0
        elseif v < -66 then
            return -60
        else
            return v + 7
        end
    else
        if v < 8 then
            return 0
        elseif v > 66 then
            return 60
        else
            return v - 7
        end
    end
end

-- frame is one indexed because Lua
local function movie_to_z(movie, frame)
    local mi = movie.inputs[frame]
    local raw_prev = nil

    if frame == 1 then
        raw_prev = movie.input_start
    else
        raw_prev = movie.inputs[frame - 1].raw
    end

    local delta = mi.pad_delta
    --[[ if reset then reset = delta & 0x0080 end ??? ]]--
    delta = bit.band(delta, bit.bnot(0x0080))

    return {
        cur = {
            input = mi.raw,
            status = 0x0000,
        },
        prev = {
            input = raw_prev,
            status = 0x0000,
        },
        press = {
            input = {
                buttons = bit.bor(bit.band(mi.raw.buttons, bit.bnot(raw_prev.buttons)), delta),
                x = mi.raw.x - raw_prev.x,
                y = mi.raw.y - raw_prev.y,
            },
            status = 0x0000,
        },
        rel = {
            input = {
                buttons = bit.bor(bit.band(bit.bnot(mi.raw.buttons), raw_prev.buttons), delta),
                x = zu_adjust_joystick(mi.raw.x),
                y = zu_adjust_joystick(mi.raw.y),
            },
            status = 0x0000,
        },
    }
end

local function set_pad_input(input)
    local buttons = expand_buttons(input.buttons)

    local bizhawk_input = {
        [CONTROLLER_PREFIX .. 'A'] = buttons.a,
        [CONTROLLER_PREFIX .. 'B'] = buttons.b,
        [CONTROLLER_PREFIX .. 'Z'] = buttons.z,
        [CONTROLLER_PREFIX .. 'Start'] = buttons.s,
        [CONTROLLER_PREFIX .. 'DPad U'] = false,
        [CONTROLLER_PREFIX .. 'DPad D'] = false,
        [CONTROLLER_PREFIX .. 'DPad L'] = false,
        [CONTROLLER_PREFIX .. 'DPad R'] = false,
        [CONTROLLER_PREFIX .. 'L'] = buttons.l,
        [CONTROLLER_PREFIX .. 'R'] = buttons.r,
        [CONTROLLER_PREFIX .. 'C Up'] = buttons.cu,
        [CONTROLLER_PREFIX .. 'C Down'] = buttons.cd,
        [CONTROLLER_PREFIX .. 'C Left'] = buttons.cl,
        [CONTROLLER_PREFIX .. 'C Right'] = buttons.cr,
    }

    joypad.set(bizhawk_input)

    joypad.setanalog({
        [CONTROLLER_PREFIX .. 'X Axis'] = input.x,
        [CONTROLLER_PREFIX .. 'Y Axis'] = input.y,
    })
end

local function reset_analog()
    joypad.setanalog({
        [CONTROLLER_PREFIX .. 'X Axis'] = '', -- janky hack m8
        [CONTROLLER_PREFIX .. 'Y Axis'] = '',
    })
end

-- z64

local function z64_vi_counter()
    return mainmemory.read_u32_be(0x009E8C)
end

local function game_state_frames()
    return mainmemory.read_u32_be(GLOBAL_CONTEXT + 0x9C)
end

-- gui

local handle = forms.newform(350, 225, 'gzm-player', function()
    running = false
    play_macro = false
    reset_analog()
end)

local movie_progress_label = forms.label(handle, 'Macro: None', 115, 15, 195, 50)

local open_button = forms.button(handle, 'Open Macro', function()
    local file = forms.openfile(nil, nil, 'GZM Macro (*.gzm)|*.gzm')
    movie = parse_macro(assert(io.open(file, 'rb')))
    forms.settext(movie_progress_label, string.format('Macro: %d frames', #movie.inputs))
end, 10, 15, 100, 25)

local play_button = forms.button(handle, 'Play Macro', function()
    if movie ~= nil then
        play_macro = true
    else
        play_macro = false
    end
end, 10, 45, 100, 25)

local stop_button = forms.button(handle, 'Stop Macro', function()
    play_macro = false
    reset_analog()
end, 10, 75, 100, 25)

-- player

while running do
    if play_macro and movie ~= nil then
        local prev_frame = nil
        local curr_frame = game_state_frames()

        for i = 1, #movie.inputs do
            if not play_macro then break end

            forms.settext(movie_progress_label, string.format('Macro: %d / %d frames', i, #movie.inputs))

            if prev_frame ~= nil then
                while prev_frame >= game_state_frames() do
                    set_pad_input(movie_to_z(movie, i).cur.input)
                    emu.frameadvance()
                end
            end

            for _ = 1, 3 do
                set_pad_input(movie_to_z(movie, i).cur.input)
                emu.frameadvance()
            end

            prev_frame = curr_frame
            curr_frame = game_state_frames()
        end

        play_macro = false
        reset_analog()
        forms.settext(movie_progress_label, string.format('Macro: %d frames', #movie.inputs))
    else
        emu.frameadvance()
    end
end
