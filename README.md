## gzm-player
A Lua script to execute [gz](https://github.com/glankk/gz) macros in [Bizhawk](http://tasvideos.org/Bizhawk.html).

### Notes
* Only supports NTSC Ocarina of Time 1.0 (English or Japanese).

### Known Issues
* Pausing the game will cause a desync.
* Entering loading zones during a macro might desync.
