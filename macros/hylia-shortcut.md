## Requirements

### Equips
* C-Down: Bombs (at least 2)
* Master Sword
* Shield

### Initial Position
* From the ice blocking Zora's Domain shortcut in Lake Hylia,
go around the right side and kill the blue Tektite, then turn
left, climb onto the platform, and sidehop left until you are
directly next to the left pillar.
