## Requirements

* Must have over half a heart.

### Equips
* C-Down: Bombs (at least 1 bomb)
* Master Sword
* Shield

### Initial Position
* Grab and then climb the ledge to the left of the Shadow Temple boss door,
turn left, roll straight forward and bonk into the wall.
