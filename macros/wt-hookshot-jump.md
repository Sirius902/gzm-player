## Requirements

* Must be at half a heart.
* Navi's drowning message must not have appeared since starting the
game.

### Equips
* C-Left: Hookshot
* C-Down: Bow (at least 1 arrow)
* Master Sword
* Shield
* Iron Boots
* Bottled Fairy

### Initial Position
* Inventory cursor must be over Iron Boots.
* Enter the swirling vortex river room in Water Temple backwards (from
the room with the giant whirlpool and dragon with a switch in its
mouth), then target the gate and sidehop right until you are in the corner.
